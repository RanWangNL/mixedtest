
#include <gtest/gtest.h>

class myTestFixture1 : public ::testing::TestWithParam<int> { 
public: 
   //myTestFixture1() { 
       // initialization code here
   //} 

   //void SetUp( ) { 
       // code here will execute just before the test ensues 
   //}

   //void TearDown( ) { 
       // code here will be called just after the test completes
       // ok to through exceptions from here if need be
   //}

   //~myTestFixture1( )  { 
       // cleanup any pending stuff, but no exceptions allowed
   //}

   // put in any custom data members that you need 
};

TEST_F(myTestFixture1, UnitTest1){ 
  ASSERT_EQ(1,1);
}

INSTANTIATE_TEST_CASE_P(Instantiation, myTestFixture1, ::testing::Range(1, 10));
