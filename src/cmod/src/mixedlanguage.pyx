import cython

cdef extern from "./include/mycpp.h":
	cdef cppclass cppprinter:
		cppprinter() except +
		void printme()


def printfromcython():
	print "Hello from Cython"
	cdef cppprinter* myprinter = new cppprinter()
	myprinter.printme()
	return None
