#include <cstdio>
#include <cstdlib>
// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>

__global__ void printfromdevice()
{
	printf("Hello from Thread %d, Block %d in Cuda\n", threadIdx.x, blockIdx.x);
}

extern "C"
{
void cudaprint(void);
}

void cudaprint(){
	printf("Hello from C\n");
	printfromdevice<<<2,2>>>();
	cudaDeviceSynchronize();
}

