CMAKE_MINIMUM_REQUIRED(VERSION 3.2)
project(CmakeTest)
find_package(CUDA REQUIRED)
find_package(Boost REQUIRED)
set(CMAKE_BUILD_TYPE release)
set(OUTPUT_DIR /home/user/anaconda3/lib/python3.5/site-packages)
add_subdirectory(cmod)
add_subdirectory(cudamod)
add_subdirectory(fmod)

set(CMAKE_MODULE_PATH /home/user/Desktop/Module)

