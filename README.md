# README #

This is a project where one use Cython, C, C++, Cuda and Fortran to create a library that can be imported in Python

This project also demonstrate how to use Gtest

#DETAILS#
1. Requires Anaconda based on Python 3.5
2. Assume Lapack and Atlas have been properly installed; is manually copied
3. Dynamic Linking is used for all libraries
4. The path of the installed python module is manually set
